#!/usr/bin/env python
# -*- coding: utf-8 -*-

def promedio(lista):
	
	suma = sum(lista) #se suman los valores de la lista
	largo = len(lista) #entrega cuantos elementos tiene
	promedio = suma/largo #divide la suma de los valores en la cantidad de valores
	print('el promedio es:', promedio) 
	
def cuadrados(lista2):
	
	#se multiplica cada valor por si mismo para que quede al cuadrado
	pri = lista2[0]*lista2[0]
	seg = lista2[1]*lista2[1]
	ter = lista2[2]*lista2[2] 
	lista_cuadrados = [pri, seg, ter] #se guardan los valores en una lista
	
	print('lista con los valores al cuadrado:', lista_cuadrados)

def cuenta_letras(lista3):
	
	#el for separara letra por letra cada palabra para despues contar cuantas letras contienen cada una
	for i in lista3:
		print()

lista = [1, 2, 6, 3.5]
print('lista 1: ', lista)

promedio(lista)


print('\n\n')


lista2 = [2, 5, 7]
print('lista 2: ', lista2)

cuadrados(lista2)


print('\n\n')
	
	
lista3 = ['que', 'como', 'cuando']
print('lista 3:', lista3)

cuenta_letras(lista3)






